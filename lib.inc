extern exit
extern string_length, string_equals, string_copy
extern print_string, print_char, print_newline, print_uint, print_int
extern read_char, read_word
extern parse_uint, parse_int
extern print_error
