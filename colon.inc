%define last_adress 0
%define dict_adress last_adress
%macro colon 2
    %ifstr %1
	%ifid %2
	   %2:
	       dq last_adress
	       db %1, 0
	       %define last_adress %2
	%else
	       %error "Incorrect lable"
	%endif
    %else
    	%error "Key must be string"
    %endif
%endmacro
