%include "lib.inc"
%include "dict.inc"

section .rodata
%include "words.inc"
not_found_msg: db "Didn't found a key", 0

section .text

%define read_length(x) mov rsi, x

%macro length_of_found_word 0
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
%endmacro

global _start
_start:
	push 0
	mov rdi, rsp
	read_length(255)
	call read_word
	mov rdi, rsp
	mov rsi, dict
	call find_word
	test rax, rax
	je .not_found
	length_of_found_word
	call print_string
	xor rdi, rdi
	jmp .end
    .not_found:
	mov rdi, not_found_msg
	call print_error
	mov rdi, 1
    .end:
	add rsp, 8
	call exit
	
