#makefile for 2 lab

ASM=nasm
ASMFLAGS=-felf64
MAKE=make
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

main: main.o lib.inc words.inc dict.inc
	$(LD) -o $@ main.o lib.o dict.o
	$(MAKE) clean
	$(MAKE) test
	

lib.inc: lib.o

words.inc: colon.inc

dict.inc: dict.o

.PHONY: clean
.PHONY: test

clean:
	rm *.o

test:
	python2 test.py
