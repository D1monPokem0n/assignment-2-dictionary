#!/usr/bin/python3

__unittest = True

import subprocess
import re
import unittest
#import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE

error_msg = "Didn't found a key"

class DictionaryTest(unittest.TestCase):
    def launch(self, input):
        output = b''
        try:
            p = Popen(['./main'], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, errors) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return (output.decode(), errors.decode())
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), exc.returncode)

    def test_find_word(self):
        d = {"1st":"1.first", "2nd": "2.second", "3rd": "3.third", "4th": "4.fourth", "5th": "2.second", "6th": "6.sixth", "7th": "7,seventh", "8th": "8.eithth", "9th": "9.nineth", "10th": "10.tenth", "11th": "11.eleventh", "12th": "12.twelfth", "mrrp": "https://www.youtube.com/watch?v=rdVcT09dEf8&ab_channel=ninerlives", "ok": "https://http.cat/status/200", "cat": "meow"}
        inputs = ['1', 'ok', 'cat', 'sdfsdf', 'Hmmm...', 'mrrp', 'ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd']
        for input in inputs:
            (output, errors) = self.launch(input)
            if (d.get(input, error_msg) != error_msg):
                self.assertEqual(output, d.get(input), 'find_word(%s) found word: %s, expected: %s' % (repr(input), repr(output), repr(d.get(input))))
            else:                        
                 self.assertEqual(errors, error_msg,  'expected an error message, but get this: %s' % (repr(errors)))

if __name__ == "__main__":
    unittest.main()
